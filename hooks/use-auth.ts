import { authAPi } from '@api/index';
import useSWR from 'swr';
import { PublicConfiguration } from 'swr/dist/types';
export function useAuth(option?: Partial<PublicConfiguration>) {
  // dung de goi profile o tat ca cac component ma ko so bi call lai api
  // profile
  const {
    data: profile,
    error,
    mutate,
  } = useSWR('/profile', {
    dedupingInterval: 60000, //1hr
    // mở tab khác thì fetch lại api, defaut: true
    revalidateOnFocus: false,
    ...option,
  });

  // check xem da login chua
  const firstLoading = profile === undefined && error === undefined;

  async function login() {
    await authAPi.login({
      username: 'test1',
      password: '123456',
    });
    //   update lai data profile theo data dc fetch
    await mutate();
  }
  async function logout() {
    await authAPi.logout();
    // update profile rỗng
    mutate(null, false);
  }
  return {
    profile,
    error,
    login,
    logout,
    firstLoading,
  };
}
