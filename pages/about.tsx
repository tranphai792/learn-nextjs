import { MainLayout } from '@/component/layout';
import Admin from '@/component/layout/admin';
import * as React from 'react';

export interface IAboutProps {}

export default function About(props: IAboutProps) {
  return (
    <div>
      <h1>About Page</h1>
    </div>
  );
}
About.Layout = Admin;

export async function getStaticProps() {
  console.log('get static props');
  return {
    props: {},
  };
}
