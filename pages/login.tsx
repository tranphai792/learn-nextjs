import { authAPi } from '@api/index';
import { useAuth } from '@hooks/index';
import { useRouter } from 'next/router';
import * as React from 'react';

export default function LoginPage() {
  const { profile, login, logout } = useAuth({ revalidateOnMount: false });
  const router = useRouter();
  const handleLogin = async () => {
    try {
      login();
      console.log('redirect to dashboard');
    } catch (error) {
      console.log('fail to login', error);
    }
  };
  // const handleGetProfile = async () => {
  //   try {
  //     await logout();
  //   } catch (error) {
  //     console.log('fail to getProfile', error);
  //   }
  // };
  const handleLogout = async () => {
    try {
      await logout();
    } catch (error) {
      console.log('fail to logout', error);
    }
  };
  return (
    <div>
      <h1>Login</h1>
      <p>Profile: {JSON.stringify(profile || {}, null, 4)}</p>
      <button onClick={handleLogin}>Login</button>
      {/* <button onClick={handleGetProfile}>Get Profile</button> */}
      <button onClick={handleLogout}>Logout</button>
      <button
        onClick={() => {
          router.push('/about');
        }}
      >
        go to about
      </button>
    </div>
  );
}
