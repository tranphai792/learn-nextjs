import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { SWRConfig } from 'swr';
import axiosClient from '@api/axios-client';
import { EmptyLayout } from '@/component/layout';
import { AppPropsWithLayout } from '@models/common';

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const Layout = Component.Layout ?? EmptyLayout;

  return (
    <SWRConfig
      value={{
        // nếu ko có fetcher truyền vào hook khác thì mặc định fetch API theo phương thức get, url truyền vào là param key trong hook
        fetcher: (url) => axiosClient.get(url),
        // mỗi lần gọi faile thì gọi lại
        shouldRetryOnError: false,
      }}
    >
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </SWRConfig>
    // setup swr toàn cục
  );
}

export default MyApp;
