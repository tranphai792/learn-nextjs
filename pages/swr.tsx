import { StudentDetail } from '@/component/swr';
import React, { useState } from 'react';

type Props = {};

const SWRPAGE = (props: Props) => {
  const [listDetail, setListDetail] = useState([1, 1, 1]);
  const handleSetList = () => {
    setListDetail((prev) => [...prev, 1]);
  };
  return (
    <div>
      <h1>SWRPAGE playground</h1>
      <button onClick={handleSetList}>ADD new detail</button>
      <ul>
        {listDetail.map((x: any, index: any) => (
          <li key={index}>
            <StudentDetail studentId="lea2aa9l7x3a5v0" />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SWRPAGE;
