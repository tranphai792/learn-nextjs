import axiosClient from '@api/axios-client';
import * as React from 'react';
import useSWR from 'swr';
export interface StudentDetailProps {
  studentId: string;
}

const MS_PER_HOUR = 60 * 60 * 1000;
export function StudentDetail({ studentId }: StudentDetailProps) {
  // với các key giống nhau và trong thời gian dedupingInterval thì useSWR sẽ không gọi lại API

  //

  // const { data, error, mutate, isValidating } = useSWR(`abc`,()=>{axiosClient.get(`/students/${studentId}`)}, {
  //   revalidateOnFocus: false,
  //   dedupingInterval: 2000,
  // });

  const { data, error, mutate, isValidating } = useSWR(`/students/${studentId}`, {
    // mở tab khác thì fetch lại api, defaut: true
    revalidateOnFocus: false,
    // thời gian chờ gọi lại
    dedupingInterval: 2000,
  });

  const handleMuntateClick = () => {
    // thay đổi dữ liệu, true: cập nhật data khi có dữ liệu mới, false sẽ lấy cố định dữ liệu fake
    mutate({ name: 'TP' }, false);
  };

  return (
    <div>
      Name: {data?.name || '--'} <button onClick={handleMuntateClick}>mutate</button>
    </div>
  );
}
/*
// mutate
//  
- Lấy dữ liệu truyền vào (tạm thời) truyền vào, thay thế dữ liệu cũ
- true: get lại dữ liệu, khi có data mới thì sẽ update
- Ứng dụng: Dùng khi update form
*/
