import { useAuth } from '@hooks/use-auth';
import { LayoutProps } from '@models/common';
import Link from 'next/link';
import { useRouter } from 'next/router';
import * as React from 'react';
import { Auth } from '../common';

export interface IAdminProps {}

export default function Admin({ children }: LayoutProps) {
  const { profile, logout } = useAuth();
  const router = useRouter();
  const handleLogout = async () => {
    await logout();
  };
  return (
    <Auth>
      <h1>Admin Layout</h1>
      <div>Sidebar</div>
      <p>Profile: {JSON.stringify(profile)}</p> <br />
      <button onClick={handleLogout}>Logout</button>
      <Link href="/">
        <a>Home</a>
      </Link>
      <Link href="/about">
        <a>About</a>
      </Link>
      <div>{children}</div>
    </Auth>
  );
}
